/**
 *
 * @copyright 2019 Extend Apps Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import {OutboundOrder} from '@extendapps/ramptypings/Documents/OutboundOrder';
import {RAMP} from '@extendapps/ramptypings/RAMP';
import * as search from 'N/search';
import {ExpectedReceipt} from '@extendapps/ramptypings/Documents/ExpectedReceipt';

// noinspection JSUnusedGlobalSymbols
export let createOutboundOrder: ICreateOutboundOrderDocument = (tranSearch: search.Search, ramp): OutboundOrder => {
    let outboundOrder: OutboundOrder = null;
    tranSearch.run().each(result => {
        if (outboundOrder === null) {
            outboundOrder = {
                WMWROOT: {
                    _xmlns: 'http://www.manh.com/ILSNET/Interface',
                    WMWDATA: {
                        Shipments: {
                            Shipment: {
                                Action: 'SAVE',
                                UserDef1: `${result.getValue(result.columns[8])}`,
                                UserDef2: `${result.id}`,
                                UserDef3: `${result.getValue(result.columns[1])}`,
                                UserDef4: `${result.getText(result.columns[2])}`,
                                UserDef5: '',
                                UserDef6: '',
                                Carrier: {
                                    Carrier: 'UPSG',
                                    Service: 'gnd'
                                },
                                Customer: {
                                    Company: ramp.CompanyCode,
                                    CustomerAddress: {
                                        Action: 'SAVE',
                                        Address1: '150 5th St',
                                        Address2: 'Suite 100',
                                        Address3: '',
                                        AttentionTo: 'The Important Folks',
                                        City: 'Huntington Beach',
                                        Consignee: '',
                                        Country: 'US',
                                        EmailAddress: 'hq@jolynclothing.com',
                                        FaxNum: '',
                                        Name: 'Jolyn',
                                        PhoneNum: '',
                                        PostalCode: '92648',
                                        ResidentialFlag: 'N',
                                        State: 'CA'
                                    },
                                    Customer: 'SHOPIFY', // Default to SHOPIFU for all shopify orders
                                    FreightBillTo: '',
                                    ShipTo: `${result.getText(result.columns[3])}`,
                                    ShipToAddress: {
                                        Action: 'SAVE',
                                        Address1: `${result.getValue(result.columns[19])}`,
                                        Address2: `${result.getValue(result.columns[20])}`,
                                        Address3: `${result.getValue(result.columns[21])}`,
                                        AttentionTo: `${result.getValue(result.columns[23])}`,
                                        City: `${result.getValue(result.columns[25])}`,
                                        Consignee: '',
                                        Country: `${result.getValue(result.columns[26])}`,
                                        EmailAddress: '',
                                        FaxNum: '',
                                        Name: `${result.getValue(result.columns[22])}`,
                                        PhoneNum: `${result.getValue(result.columns[27])}`,
                                        PostalCode: `${result.getValue(result.columns[29])}`,
                                        ResidentialFlag: 'F',
                                        State: `${result.getValue(result.columns[28])}`,
                                    }
                                },
                                ErpOrder: `${result.getValue(result.columns[4])}`,
                                FreightTerms: '',
                                OrderDate: `${result.getValue(result.columns[5])}`,
                                OrderType: 'WEB',
                                PlannedShipDate: `${result.getValue(result.columns[6])}`,
                                Priority: '99',
                                RequestedDeliveryDate: `${result.getValue(result.columns[6])}`,
                                RequestedDeliveryType: 'By',
                                ScheduledShipDate: '',
                                ShipmentId: `${ramp.CompanyCode}-${result.getValue(result.columns[4])}`,
                                UserDef11: '',
                                UserDef13: '',
                                UserDef14: '',
                                Warehouse: ramp.WarehousCode,
                                Details: {
                                    ShipmentDetail: []
                                }
                            }
                        }
                    }
                }
            };
        }

        outboundOrder.WMWROOT.WMWDATA.Shipments.Shipment.Details.ShipmentDetail.push({
            Action: 'SAVE',
            UserDef4: `${result.getValue(result.columns[16])}`,
            ErpOrder: `${result.getValue(result.columns[4])}`,
            ErpOrderLineNum: `${result.getValue(result.columns[15])}`,
            SKU: {
                Company: ramp.CompanyCode,
                Desc: `${result.getValue(result.columns[30])}`,
                Item: `${result.getText(result.columns[12])}`,
                NetPrice: `${result.getValue(result.columns[13])}`,
                Quantity: `${result.getValue(result.columns[18])}`,
                QuantityUm: 'EA',
                Whs: ramp.WarehousCode
            }
        });

        return true;
    });

    return outboundOrder;
};

export let createExpectedReceipt: ICreateExpectedReceiptDocument = (tranSearch: search.Search, ramp): ExpectedReceipt => {
    let expectedReceipt: ExpectedReceipt = null;
    tranSearch.run().each(result => {
        if (expectedReceipt === null) {
            expectedReceipt = {
                WMWROOT: {
                    _xmlns: 'http://www.manh.com/ILSNET/Interface',
                    WMWDATA: {
                        Receipts: {
                            PurchaseOrder: {
                                Action: 'SAVE',
                                UserDef1: `${result.id}`,
                                Company: ramp.CompanyCode,
                                PurchaseOrderId: `${result.getValue(result.columns[1])}`,
                                ReceiptType: 'PO',
                                Warehouse: ramp.WarehousCode,
                                Details: {
                                    PurchaseOrderDetail: []
                                }
                            }
                        }
                    }
                }
            };
        }

        expectedReceipt.WMWROOT.WMWDATA.Receipts.PurchaseOrder.Details.PurchaseOrderDetail.push({
            Action: 'SAVE',
            LineNumber: +result.getValue(result.columns[2]),
            SKU: {
                Company: ramp.CompanyCode,
                Desc: `${result.getValue(result.columns[6])}`,
                Item: `${result.getText(result.columns[3])}`,
                NetPrice: `${result.getValue(result.columns[4])}`,
                Quantity: `${result.getValue(result.columns[5])}`,
                QuantityUm: 'EA',
                Whs: ramp.WarehousCode
            }
        });

        return true;
    });
    return expectedReceipt;
};

interface ICreateOutboundOrderDocument {
    // noinspection JSUnusedLocalSymbols
    (tranSearch: search.Search, ramp: RAMP): OutboundOrder;
}

interface ICreateExpectedReceiptDocument {
    // noinspection JSUnusedLocalSymbols
    (tranSearch: search.Search, ramp: RAMP): ExpectedReceipt;
}

export interface RAMPDocumentPlugIn {
    createOutboundOrder: ICreateOutboundOrderDocument;
    createExpectedReceipt: ICreateExpectedReceiptDocument;
    createProductMaster: any;
}