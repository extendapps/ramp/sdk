/**
 *
 * @copyright 2019 Extend Apps Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';

export let sendDocument: ISendDocument = (options: SendDocumentOptions) => {
    log.audit('sendDocumeent - options', options);
    return true;
};

interface ISendDocument {
    (options: SendDocumentOptions);
}

interface SendDocumentOptions {
    fileId: number;
    Sent?: () => void;
    Failed?: () => void;
}

export interface SendDocumentPlugIn {
    sendDocument: ISendDocument;
}